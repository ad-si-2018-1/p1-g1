net = require('net')


var porta = 3000;

net.createServer(function(socket){

	var clients = []

	var nicks = []

	var canais = {}

	var serverName = 'localhost';

	var motdMsg = "Bem-vindo ao IRC Server: ADSI-P1-G1";
	

	var onJoin = function(data){
		
		var canal = String(String(data).split(" ")[1]).toLowerCase().trim();
		
		if(canais[canal] === undefined){
			canais[canal] = {clientesCanal: {}, clientesArray: []};
		}

		if(canais[canal].clientesCanal[String(socket.Nick)] !== undefined){
			return;
		}
		
		console.log("Nick no join: ", socket.Nick);
		canais[canal].clientesCanal[String(socket.Nick)] = socket;
		canais[canal].clientesArray.push(String(socket.Nick));
		if(socket.CanaisInscritos === undefined){
			socket.CanaisInscritos = [String(canal)];
		}
		else {
			socket.CanaisInscritos.push(String(canal));
		}
		
		broadcast(canal, socket.Nick + " entrou no canal " + canal);

		socket.write("Você entrou no canal: " + data + "\n");
	};

	var onPrivMsg = function(socket, data){

		var nickTarget = String(data).split(" ")[1];
		var mensagem = String(data).split(" ")[2];

		console.log("mensagem: ", mensagem);
		console.log("nickTarget: ", nickTarget);

		if(client.nick){
			console.log("client.nick encontrado: ", client.nick);
			client.write(socket.nick + " > " + mensagem);
		} else {
			socket.write("Usuário não encontrado\n");
		}
	};

	var onUser = function(socket, data){
		var nick = String(String(data).split(" ")[1]).toLowerCase().trim();
		var modo = String(String(data).split(" ")[2]).toLowerCase().trim();
		var realName = String(String(data).split(":")[1]).trim().split(" ");

		
		if(!nick || !modo || !realName){
			socket.write("ERR_NEEDMOREPARAMS\n")
		} else if(nicks[nick.toString()] !== undefined){
			socket.write("ERR_ALREADYREGISTRED\n")

		}
		else
		{
			socket.name = nick;
			socket.mode = modo;
			socket.realname = realName;
			socket.CanaisInscritos = [];
	    		
			motd(socket);

			socket.write("comando User com sucesso\n");
		}			

	};

	var onNick = function(data){
		var novoNick = String(String(data).split(" ")[1]).trim();
		console.log("Data no nick: ", String(data));
		if(!novoNick || novoNick === ""){
			socket.write("ERR_NEEDMOREPARAMS\n")
		}
		else {
			clients[String(novoNick)] = socket;
			nicks.push(novoNick);
			console.log("Nick no nick: ", novoNick);
			socket.Nick = novoNick
			socket.write("comando Nick com sucesso\n")
			console.log('nicks: ', nicks);
		}
	};
	
	

	//array associativos para identificação e chamada dos métodos através dos seus nomes
	comandos = {
		"user" 		: onUser,
		"nick" 		: onNick,
		"join"		: onJoin,
		"privmsg" 	: onPrivMsg
	}

	socket.id = socket.remoteAddress + ":" + socket.remotePort;

	clients.push(socket);

	socket.on('data', function (data) {
		analisarData(data)
	});

	socket.on('end', function () {
		clients.splice(clients.indexOf(socket), 1);
		broadcast(socket.nick + " saiu do chat.\n");
	});

	function analisarData(data){
		var comando = String(data).split(" ")[0].toLowerCase().trim();

		if(comandos[comando] === undefined)
			socket.write("Comando não existe\n")
		else
			comandos[comando](data)

	};	
	
	function broadcast (canal, msg){

		console.log(JSON.stringify(canais[canal].clientesCanal));

		for(var cliente in canais[canal].clientesCanal){
			console.log('cliente: ', String(cliente.Nick));
		}
			
		//canais[canal].clientesArray.forEach(function(cliente, nicks) {
		//	console.log('cliente: ', String(cliente));
		//	console.log('comprimento nicks aqui: ', nicks.length);
		//	//nicks[nick].write(String(msg));
		//});
	};	

	function motd(socket) {
		console.log(String(motdMsg));
		if (String(motd).length > 0) {
			var motdStart = ":" + serverName + " " + socket.nick + " :- " + "localhost" + " Mensagem do Dia- \r\n";
			var messageOfTheDay = ":" + serverName + " " + socket.nick + " :- " + motdMsg + "\r\n";
			var endOfMotd = ":" + serverName + " " + socket.nick + " :Final da mensagem do dia\r\n";
			socket.write(motdStart);
			socket.write(messageOfTheDay);
			socket.write(endOfMotd);
		}
		else {
			socket.write(":" + serverName + " " + socket.nick + " :MOTD Mensagem do dia não disponível\r\n");
		}
	};

	
	console.log("Chat server rodando na porta: " + porta + " \n");
}).listen(porta);

console.log("IRC Server rodando na porta: " + porta + " \n");
