# Manual do Desenvolvedor

## Histórico de Alterações

**Data** | **Descrição** | **Autor**
:---:|:---------:|:-------:
10/04/18 | Inicio do Manual | Victor Murilo

## Sumário

* [Introdução] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#1-introdu%C3%A7%C3%A3o)
    * [Público Alvo] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#11-p%C3%BAblico-alvo)
* [Mensagens Suportadas] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#2-mensagens-suportadas)
* [Mensagens Não Suportadas] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#3-mensagens-não-suportadas)
* [Diagramas] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#4-diagramas)
* [Testes Realizados] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#5-testes-realizados)
* [Referências e Materiais Externos] (https://gitlab.com/ad-si-2018-1/p1-g1/tree/master/Manual%20do%20Desenvolvedor%20/#6-referencias-e-materiais-externo)

***

### 1. Introdução

#### 1.1. Público Alvo
<p>As informações deste manual são destinadas aos desenvolvedores de software, analistas programadores e 
público em geral interessado em aprender como se utilizar este material de uso do IRC. 
</p>

***

### 2. Mensagens Suportadas

PASS

USER

NICK

JOIN

PART

MODE Usuário

MODE Canal

Tópicos - Temas do canal

NAMES

LIST

INVITE

KICK

QUIT

PRIMVSG

MOTD

WHO

PING

USERHOST

WHOIS

WHOWAS

LUSERS

TIME
***

### 3. Mensagens não Suportadas

***

### 4. Diagramas

***

### 5. Testes Realizados

***

### 6. Referencias e Materiais Externos

<a href="https://www.cielo.com.br/wps/wcm/connect/c682298e-4518-4e2b-8945-cef23e04b5ec/Cielo-E-commerce-Manual-do-Desenvolvedor-WebService-PT-V2.5.4.pdf?MOD=AJPERES&CONVERT_TO=url?utm_source=SECNET&utm_campaign=Checkout%20Transparente%20e%20Certificado%20SS">Manual do Desenvolvedor - CIELO</a>

***